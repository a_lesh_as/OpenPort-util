using System.Net;
using System.Net.Sockets;
using Logger;
using OpenPort.extensions;

namespace OpenPort
{
    public class TcpScanner : IPortScanner
    {
        private readonly ILogger logger = LoggerManager.Logger;

        public async Task<PortStatus> GetPortStatus(IPEndPoint endPoint, TimeSpan timeout)
        {
            using var client = new TcpClient();
            var connectStatus = await client.ConnectWithTimeoutAsync(endPoint, timeout);

            var portStatus = connectStatus switch
            {
                TaskStatus.RanToCompletion => PortStatus.OPEN,
                _ => PortStatus.CLOSED
            };
            if (portStatus == PortStatus.OPEN)
                logger.Info(
                    $"(TcpScanner): checked {endPoint}:{portStatus} at thread {Thread.GetCurrentProcessorId()}"
                );

            return portStatus;
        }
    }
}