using System.Net;
using System.Net.Sockets;

namespace OpenPort.extensions;

internal static class TcpClientExtensions
{
    public static async Task<TaskStatus> ConnectWithTimeoutAsync(
        this TcpClient client,
        IPEndPoint endPoint,
        TimeSpan timeout
    )
    {
        var connectTask = client.ConnectAsync(endPoint);
        await Task.WhenAny(connectTask, Task.Delay(timeout));
        return connectTask.Status;
    }
}