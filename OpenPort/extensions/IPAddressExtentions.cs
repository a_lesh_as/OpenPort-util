using System.Net;
using System.Net.NetworkInformation;

namespace OpenPort.extensions;

internal static class IpAddressExtensions
{
    public static async Task<IPStatus> Ping(this IPAddress ipAddress, int timeout = 3000)
    {
        using var ping = new Ping();
        var pingReq = (await ping.SendPingAsync(ipAddress, timeout)).Status;
        return pingReq;
    }
}