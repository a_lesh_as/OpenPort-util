namespace OpenPort.extensions
{
    internal static class RangeExtensions
    {
        public static IEnumerable<int> ToEnumerable(this Range range) =>
            Enumerable.Range(range.Start.Value, range.End.Value - range.Start.Value + 1);
    }
}