using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Logger;
using OpenPort.extensions;

namespace OpenPort;

public class UdpScanner : IPortScanner
{
    private readonly ILogger logger = LoggerManager.Logger;
    private static byte[] message = Encoding.ASCII.GetBytes("hello, world!");

    public async Task<PortStatus> GetPortStatus(IPEndPoint endPoint, TimeSpan timeout)
    {
        PortStatus portStatus;
        try
        {
            using var client = new UdpClient();
            await Task.Run(() => client.Connect(endPoint));
            await client.SendAsync(message, message.Length);
            var receiveTask = Task.Run(() => client.Receive(ref endPoint));
            await Task.WhenAny(receiveTask, Task.Delay(timeout));
            portStatus = receiveTask.Status switch
            {
                TaskStatus.Running => PortStatus.OPEN,
                _ => PortStatus.CLOSED
            };
        }
        catch (Exception e)
        {
            portStatus = PortStatus.CLOSED;
            logger.Info($"(UdpScanner): ICMP error on port - {endPoint.Port}:\n {e.Message}");
        }

        if (portStatus == PortStatus.OPEN)
            logger.Info(
                $"(UdpScanner): checked {endPoint}:{portStatus} at thread {Thread.GetCurrentProcessorId()}"
            );

        return portStatus;
    }
}