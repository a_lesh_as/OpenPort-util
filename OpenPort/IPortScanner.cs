using System.Net;
using System.Net.NetworkInformation;
using OpenPort.extensions;

namespace OpenPort
{
    public interface IPortScanner
    {
        async Task<IEnumerable<int>> Scan(
            IPAddress ipAddress,
            Range portRange,
            TimeSpan timeout,
            PortStatus status)
        {
            if (await ipAddress.Ping() != IPStatus.Success)
                return Array.Empty<int>();

            var tasks = portRange.ToEnumerable()
                .Select(p => GetPortStatus(new IPEndPoint(ipAddress, p), timeout))
                .ToArray();
            await Task.WhenAll(tasks);
            return tasks
                .Select((x, i) => (x, i + portRange.Start.Value))
                .Where(x => x.x.Result == status)
                .Select(x => x.Item2);
        }

        Task<PortStatus> GetPortStatus(IPEndPoint endPoint, TimeSpan timeout);
    }
}