namespace Logger;

public static class LoggerManager
{
    public static ILogger Logger => logger.Value;
    
    private static Lazy<ILogger> logger;

    static LoggerManager()
    {
        logger = new Lazy<ILogger>(() => new Logger("trace.log"));
    }
}