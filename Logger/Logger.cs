namespace Logger;

public class Logger : ILogger
{
    private readonly object lockObject;
    private readonly string logPath; 
    
    public Logger(string logPath)
    {
        lockObject = new object();
        this.logPath = logPath;
    }
    
    public void Info(string message)
    {
        lock (lockObject)
        {
            using var writer = (TextWriter) File.AppendText(logPath);
            writer.WriteLine(message);
        }
    }
}