﻿using System.Net;
using OpenPort;
using CommandLine;
using ConsoleUI;
using ConsoleUI.parsers;
using ConsoleUI.validators;

internal class Program
{
    public static void Main(string[] args)
    {
        Parser.Default.ParseArguments<ProgramOptions>(args)
            .WithParsed(ParseFunc);
    }

    private static void ParseFunc(ProgramOptions options)
    {
        Validate(options);
        IPortScanner scanner = options.Protocol.Equals("tcp") ? new TcpScanner() : new UdpScanner();
        const PortStatus status = PortStatus.OPEN;
        try
        {
            var ports = scanner.Scan(
                IPAddress.Parse(options.IpAddress),
                RangeParser.Get().Parse(options.Range),
                options.Timeout,
                status
            ).Result.ToArray();
            PrintResult(ports, status, options.IpAddress);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }

    private static void Validate(ProgramOptions options)
    {
        try
        {
            ProtocolValidator.Get().Validate(options.Protocol);
            RangeValidator.Get().Validate(options.Range);
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
            throw;
        }
    }

    private static void PrintResult(int[] ports, PortStatus status, string ip)
    {
        Console.WriteLine(ports.Length == 0
            ? "No such ports"
            : string.Join('\n', ports.Select(x => $"{ip}:{x}:{status}")));
    }
}