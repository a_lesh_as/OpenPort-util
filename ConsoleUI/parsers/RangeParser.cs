using System.Text.RegularExpressions;

namespace ConsoleUI.parsers;

public class RangeParser : Singleton<RangeParser>, IParser<Range>
{
    private static Regex regex = new(@"\d+");

    public Range Parse(string input)
    {
        var nums = regex.Matches(input).Select(x => int.Parse(x.Value)).ToArray();
        if (nums[0] > nums[1] || nums[0] < 1 || nums[1] > 65535)
            throw new ArgumentException("Invalid range (min - 1, max - 65535), use --help");
        return new Range(nums[0], nums[1]);
    }
}