namespace ConsoleUI.parsers;

public interface IParser<out T>
{
    T Parse(string input);
}