using CommandLine;

namespace ConsoleUI;

internal class ProgramOptions
{
    [Option('p', "protocol", Required = true, HelpText = "Set protocol[tcp or upp], example: tcp")]
    public string Protocol { get; set; }
    
    [Option('i', "ip-address", Required = true, HelpText = "Set ip-address, example: 127.0.0.0")]
    public string IpAddress { get; set; }

    [Option('r', "range", Required = true, HelpText = "Set range for ports, example: 2,1000")]
    public string Range { get; set; }
    
    [Option('t', "timeout", Required = false, HelpText = "Set time on response waiting, example(default): 00:00:03")]
    public TimeSpan Timeout { get; set; } = TimeSpan.FromSeconds(3);
}