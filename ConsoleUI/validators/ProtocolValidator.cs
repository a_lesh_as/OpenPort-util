namespace ConsoleUI.validators;

public class ProtocolValidator : Singleton<ProtocolValidator>, IValidator
{
    private static readonly HashSet<string> availableProtocols = new() {"tcp", "udp"};

    public void Validate(string data)
    {
        if (!availableProtocols.Contains(data))
            throw new ArgumentException("Incorrect input protocol, use --help");
    }
}