namespace ConsoleUI.validators;

public interface IValidator
{
    void Validate(string data);
}