using System.Text.RegularExpressions;

namespace ConsoleUI.validators;

public class RangeValidator : Singleton<RangeValidator>, IValidator
{
    private static readonly Regex regex = new(@"\d+,\d+");

    public void Validate(string data)
    {
        if (!regex.IsMatch(data))
            throw new ArgumentException("Incorrect range line, use --help");
    }
}