using System.Reflection;

namespace ConsoleUI;

public class Singleton<T>
    where T : class
{
    private static readonly Lazy<T> instance;

    static Singleton()
    {
        instance = new Lazy<T>(
            () => (Activator.CreateInstance(typeof(T)) as T)!
        );
    }

    public static T Get() => instance.Value;
}