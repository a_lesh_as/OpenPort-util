# OpenPort Util

Утилита для чекига TCP/UDP портов, пробегается по заданному диапазону портов, в случае TCP - считает порт открытым, если `получилось установить соединение`, в случае UDP - `отсутствие ICMP ошибки`. 

Пример использования:

```
./openport -i 127.0.0.1 -p tcp -r 1,1000
```

![image.png](image.png)


Если хотите почитать про каждую опцию, напишите -
```
./openport --help
``` 
